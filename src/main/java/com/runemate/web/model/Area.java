package com.runemate.web.model;

import com.runemate.web.vertex.*;
import lombok.*;

@RequiredArgsConstructor
public class Area {

    private final int x, y, width, height;
    private int plane = 0;

    public Area(final int x, final int y, final int width, final int height, final int plane) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.plane = plane;
    }

    public Coordinate topRight() {
        return new Coordinate(x + width - 1, y + height - 1, plane);
    }

    public Coordinate bottomLeft() {
        return new Coordinate(x, y, plane);
    }

    public boolean contains(@NonNull final Vertex vertex) {
        return vertex.x() >= x && vertex.y() >= y && vertex.x() < x + width && vertex.y() < y + height;
    }
}
