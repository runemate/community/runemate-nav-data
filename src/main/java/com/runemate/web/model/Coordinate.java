package com.runemate.web.model;

import com.runemate.web.vertex.*;
import lombok.*;

@Value
public class Coordinate {

    int x, y, plane;

    public Coordinate(int x, int y) {
        this(x, y, 0);
    }

    public Coordinate(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.plane = z;
    }

    public Vertex asVertex() {
        return new VertexLiteral(x, y, plane);
    }

    public Coordinate derive(int x, int y, int plane) {
        return new Coordinate(this.x + x, this.y + y, this.plane + plane);
    }
}
