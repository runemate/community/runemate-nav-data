package com.runemate.web.vertex;

import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class VertexLiteral extends VertexBase {

    int x;
    int y;
    int z;

    public VertexLiteral(int x, int y) {
        this(x, y, 0);
    }

    public static VertexLiteral derive(@NonNull Vertex base, int dx, int dy, int dz) {
        return new VertexLiteral(base.x() + dx, base.y() + dy, base.z() + dz);
    }
}
