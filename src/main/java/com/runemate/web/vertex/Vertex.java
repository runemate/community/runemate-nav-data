package com.runemate.web.vertex;

public /*sealed*/ interface Vertex /*permits VertexBase*/ {

    int x();

    int y();

    int z();

    Vertex WILDCARD = new VertexLiteral(-1, -1, -1);
}
