package com.runemate.web.vertex;

import java.util.*;

public abstract class VertexBase implements Vertex {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vertex)) {
            return false;
        }
        Vertex that = (Vertex) o;
        return x() == that.x() && y() == that.y() && z() == that.z();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = x();
        result = prime * result + y();
        result = prime * result + z();
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Vertex.class.getSimpleName() + "[", "]")
            .add("x=" + x())
            .add("y=" + y())
            .add("z=" + z())
            .toString();
    }
}
