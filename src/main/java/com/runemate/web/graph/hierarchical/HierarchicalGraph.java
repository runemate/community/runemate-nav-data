package com.runemate.web.graph.hierarchical;

import com.runemate.web.graph.*;
import com.runemate.web.vertex.*;
import lombok.*;
import org.jetbrains.annotations.*;

public /*sealed*/ interface HierarchicalGraph<G extends Graph<V>, V> extends Graph<G> /*permits HierarchicalGraphBase*/ {

    @Nullable G sub(@NonNull V vertex);

    @NotNull Graph<Vertex> flatten();
}
