package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class SkillRequirement implements Requirement {

    Skill skill;
    int level;

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasSkillLevel(skill, level);
    }

    public static SkillRequirement of(Skill skill, int level) {
        return new SkillRequirement(skill, level);
    }

    @Override
    public int type() {
        return Type.SKILL;
    }

    @Getter
    @AllArgsConstructor
    public enum Skill {
        ATTACK(0),
        DEFENCE(1),
        STRENGTH(2),
        CONSTITUTION(3),
        RANGED(4),
        PRAYER(5),
        MAGIC(6),
        COOKING(7),
        WOODCUTTING(8),
        FLETCHING(9),
        FISHING(10),
        FIREMAKING(11),
        CRAFTING(12),
        SMITHING(13),
        MINING(14),
        HERBLORE(15),
        AGILITY(16),
        THIEVING(17),
        SLAYER(18),
        FARMING(19),
        RUNECRAFTING(20),
        HUNTER(21),
        CONSTRUCTION(22);
        final int index;

        public SkillRequirement required(int level) {
            return new SkillRequirement(this, level);
        }
    }
}
