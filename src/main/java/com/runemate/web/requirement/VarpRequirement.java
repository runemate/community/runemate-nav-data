package com.runemate.web.requirement;

import com.runemate.web.*;
import com.runemate.web.util.*;
import lombok.*;
import lombok.experimental.*;

@Value
public class VarpRequirement implements Requirement {

    int varpIndex;
    int varpValue;
    IntOp op;

    @Tolerate
    public VarpRequirement(final int varpIndex, final int varpValue) {
        this(varpIndex, varpValue, IntOp.EQ);
    }

    @Override
    public boolean satisfy(@NonNull WebContext context) {
        return context.hasVarp(varpIndex, varpValue, op);
    }

    @Override
    public int type() {
        return Type.VARP;
    }
}
