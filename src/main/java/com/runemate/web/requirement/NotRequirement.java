package com.runemate.web.requirement;

import com.runemate.web.*;
import lombok.*;

@Value
public class NotRequirement implements Requirement {

    Requirement requirement;

    NotRequirement(@NonNull Requirement requirement) {
        this.requirement = requirement;
    }

    @Override
    public boolean satisfy(@NonNull final WebContext context) {
        return !requirement.satisfy(context);
    }

    @Override
    public int type() {
        return Type.NOT;
    }
}
