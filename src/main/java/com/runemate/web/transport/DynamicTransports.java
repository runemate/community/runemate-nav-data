package com.runemate.web.transport;

import com.runemate.web.*;
import com.runemate.web.data.*;
import com.runemate.web.transport.dynamic.*;
import java.util.*;
import lombok.experimental.*;

@UtilityClass
public class DynamicTransports {

    public Collection<DynamicTransport> load(WebContext context) {
        final List<DynamicTransport> transports = new ArrayList<>();

        transports.addAll(Items.getAll(context));
        transports.addAll(Spell.getAll(context));

        return transports;
    }

    public Collection<DynamicTransport> load() {
        final List<DynamicTransport> transports = new ArrayList<>();

        transports.addAll(Items.getAll());
        transports.addAll(Spell.getAll());

        return transports;
    }

}
