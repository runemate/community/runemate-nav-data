package com.runemate.web.transport.dynamic;

import java.util.regex.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString
public class BasicItemTransport extends DynamicTransport {

    @NonNull Pattern itemName;
    @NonNull Pattern itemAction;
    @NonNull Origin origin;

    public enum Origin {
        EQUIPMENT,
        INVENTORY
    }
}
