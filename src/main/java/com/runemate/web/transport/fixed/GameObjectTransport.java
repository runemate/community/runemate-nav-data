package com.runemate.web.transport.fixed;

import com.runemate.web.model.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@SuperBuilder(toBuilder = true)
@ToString(callSuper = true)
public class GameObjectTransport extends FixedTransport {

    @NonNull Coordinate objectPosition;
    @NonNull String objectName;
    @NonNull String objectAction;
    @Builder.Default boolean bidirectional = false;

}
