package com.runemate.web.util;

import java.util.function.*;
import lombok.*;
import org.jetbrains.annotations.*;

@AllArgsConstructor
public enum IntOp implements BiFunction<Integer, Integer, Boolean> {
    EQ(Integer::equals),
    NEQ((x, y) -> !x.equals(y)),
    GT((x, y) -> x > y),
    GTE((x, y) -> x >= y),
    LT((x, y) -> x < y),
    LTE((x, y) -> x <= y),
    TRUE((x, y) -> true);

    private final BiFunction<Integer, Integer, Boolean> test;

    @NonNull
    @Override
    public Boolean apply(@Nullable Integer left, @Nullable Integer right) {
        return left != null && right != null ? test.apply(left, right) : false;
    }

}
