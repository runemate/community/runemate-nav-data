package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.util.*;
import com.runemate.web.vertex.*;
import java.util.*;
import java.util.regex.*;
import lombok.experimental.*;

/**
 * Many underworld objects are added automatically by the graph generator, but it has no way of determining if the object has
 * any requirements to use.
 * <p>
 * This class aims to remedy that by providing a means to pre-define requirements for specific object positions.
 */
@UtilityClass
public class RequirementOverrides {

    private final Area DWARVEN_MINE_OVERWORLD = new Area(3017, 3337, 4, 4);
    private final Set<Vertex> FARMING_GUILD_DOORS = Set.of(
        new VertexLiteral(1248, 3723, 0),
        new VertexLiteral(1249, 3723, 0)
    );
    private final Vertex DUSTY_KEY_GATE = new VertexLiteral(2924, 9803, 0);
    private final Vertex BRASS_KEY_GATE = new VertexLiteral(3115, 3450, 0);

    public static Requirement override(final Vertex vertex) {
        if (DWARVEN_MINE_OVERWORLD.contains(vertex)) {
            return SkillRequirement.Skill.MINING.required(60);
        }

        if (FARMING_GUILD_DOORS.contains(vertex)) {
            return SkillRequirement.Skill.FARMING.required(45);
        }

        if (DUSTY_KEY_GATE.equals(vertex)) {
            return new ItemRequirement(Pattern.compile("Dusty key"), 1);
        }

        if (BRASS_KEY_GATE.equals(vertex)) {
            return new ItemRequirement(Pattern.compile("Brass key"), 1);
        }

        return Requirements.none();
    }

}
