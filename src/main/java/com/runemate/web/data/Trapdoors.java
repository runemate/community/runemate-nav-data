package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import lombok.experimental.*;

@UtilityClass
public class Trapdoors {

    public Collection<FixedTransport> getAll() {
        return Arrays.asList(
            //Tower of life basement
            TrapdoorTransport.builder()
                .trapdoorPosition(new Coordinate(2648, 3212, 0))
                .ladderPosition(new Coordinate(3038, 4375, 0))
                .source(new Coordinate(2649, 3212, 0))
                .destination(new Coordinate(3038, 4376, 0))
                .requirement(QuestRequirement.complete("TOWER_OF_LIFE"))
                .build()
        );
    }

}
