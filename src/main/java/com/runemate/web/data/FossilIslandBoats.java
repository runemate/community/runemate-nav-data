package com.runemate.web.data;

import com.runemate.web.model.Coordinate;
import com.runemate.web.requirement.QuestRequirement;
import com.runemate.web.requirement.SkillRequirement;
import com.runemate.web.transport.fixed.DialogGameObjectTransport;
import com.runemate.web.transport.fixed.FixedTransport;
import com.runemate.web.transport.fixed.GameObjectTransport;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

@UtilityClass
public class FossilIslandBoats {

    public Collection<FixedTransport> getAll() {
        final List<FixedTransport> transports = new ArrayList<>();

        // Camp boat to north island
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3724, 3808, 0))
                .objectPosition(new Coordinate(3723, 3805, 0))
                .destination(new Coordinate(3769, 3898, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row out to sea, north of the island.*"))
                .build());

        // Camp boat to north of fossil island
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3724, 3808, 0))
                .objectPosition(new Coordinate(3723, 3805, 0))
                .destination(new Coordinate(3733, 3893, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row to the north of the island.*"))
                .build());

        // Camp boat to Digsite
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3724, 3808, 0))
                .objectPosition(new Coordinate(3723, 3805, 0))
                .destination(new Coordinate(3362, 3445, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row to the barge and travel to the Digsite.*"))
                .build());

        // North boat to camp
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3733, 3893, 0))
                .objectPosition(new Coordinate(3734, 3894, 0))
                .destination(new Coordinate(3724, 3808, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row to the camp.*"))
                .build());

        // North boat to sea
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3733, 3893, 0))
                .objectPosition(new Coordinate(3734, 3894, 0))
                .destination(new Coordinate(3769, 3898, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row to sea.*"))
                .build());

        // Sea boat to north of island
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3769, 3898, 0))
                .objectPosition(new Coordinate(3766, 3897, 0))
                .destination(new Coordinate(3733, 3893, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row to the north of the island.*"))
                .build());

        // Sea boat to camp
        transports.add(DialogGameObjectTransport.builder()
                .source(new Coordinate(3769, 3898, 0))
                .objectPosition(new Coordinate(3766, 3897, 0))
                .destination(new Coordinate(3724, 3808, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                .requirement(QuestRequirement.complete("BONE_VOYAGE"))
                .dialogPattern(Pattern.compile("Row to the camp.*"))
                .build());

        // Sea boat to underwater
        // Not supported due to underwater not being mapped

        // Western boat to Lithkren
        transports.add(GameObjectTransport.builder()
                .source(new Coordinate(3660, 3849, 0))
                .objectPosition(new Coordinate(3658, 3848, 0))
                .destination(new Coordinate(3583, 3973, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                // DS2 requires Bone Voyage completed
                .requirement(QuestRequirement.complete("DRAGON_SLAYER_II"))
                .build());

        // Lithkren boat to western fossil island
        transports.add(GameObjectTransport.builder()
                .source(new Coordinate(3583, 3973, 0))
                .objectPosition(new Coordinate(3582, 3971, 0))
                .destination(new Coordinate(3660, 3849, 0))
                .objectName("Rowboat")
                .objectAction("Travel")
                // DS2 requires Bone Voyage completed
                .requirement(QuestRequirement.complete("DRAGON_SLAYER_II"))
                .build());

        return transports;
    }
}