package com.runemate.web.data;

import com.runemate.web.model.*;
import java.util.*;
import lombok.*;

public class GrandExchangeClerks {

    @Getter(lazy = true)
    private static final List<Coordinate> all = List.of(new Coordinate(3164, 3487, 0),
        new Coordinate(3165, 3487, 0),
        new Coordinate(3164, 3492, 0),
        new Coordinate(3165, 3492, 0)
    );
}
