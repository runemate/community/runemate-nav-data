package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.fixed.*;
import java.util.*;
import lombok.*;

@Getter
@RequiredArgsConstructor
public enum SpiritTree {

    TREE_GNOME_VILLAGE(
        "Tree Gnome Village",
        new Coordinate(2542, 3170, 0),
        new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE)
    ),
    GNOME_STRONGHOLD(
        "Gnome Stronghold",
        new Coordinate(2461, 3444, 0),
        new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE).and(new QuestRequirement(
            "THE_GRAND_TREE",
            QuestRequirement.State.COMPLETE
        ))
    ),
    KHAZARD(
        "Battlefield of Khazard",
        new Coordinate(2555, 3259, 0),
        new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE)
    ),
    GRAND_EXCHANGE(
        "Grand Exchange",
        new Coordinate(3185, 3508, 0),
        new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE)
    ),
    FELDIP_HILLS(
        "Feldip Hills",
        new Coordinate(2488, 2850, 0),
        new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE)
    ),
    PRIFDDINAS(
        "Prifddinas",
        new Coordinate(3274, 6124, 0),
        new QuestRequirement("TREE_GNOME_VILLAGE", QuestRequirement.State.COMPLETE).and(new QuestRequirement(
            "SONG_OF_THE_ELVES",
            QuestRequirement.State.COMPLETE
        ))
    );

    private final String location;
    private final Coordinate position;
    private final Requirement requirement;

    public static List<SpiritTreeTransport> transports() {
        final var results = new ArrayList<SpiritTreeTransport>();
        for (final var source : values()) {
            for (final var destination : values()) {
                if (source.equals(destination)) {
                    continue;
                }
                results.add(SpiritTreeTransport.builder()
                    .source(source.getPosition())
                    .destination(destination.getPosition())
                    .location(destination.getLocation())
                    .requirement(destination.getRequirement())
                    .build());
            }
        }
        return results;
    }
}
