package com.runemate.web.data;

import com.runemate.web.model.*;
import com.runemate.web.requirement.*;
import com.runemate.web.transport.dynamic.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;

@Getter
public enum ChargedItem {
    RING_OF_DUELING("Ring of dueling", 20, new Destination[] {
        new Destination("PvP Arena", new Coordinate(3315, 3235, 0)),
        new Destination(Pattern.compile("Castle Wars(?: Arena)?"), new Coordinate(2440, 3090, 0)),
        new Destination("Ferox Enclave", new Coordinate(3151, 3635, 0))
    }),
    GAMES_NECKLACE("Games necklace", 20, new Destination[] {
        new Destination("Burthorpe", new Coordinate(2898, 3553, 0)),
        new Destination("Barbarian Outpost", new Coordinate(2520, 3571, 0)),
        new Destination("Corporeal Beast", new Coordinate(2964, 4382, 2)),
        new Destination("Tears of Guthix", new Coordinate(3244, 9501, 2), QuestRequirement.complete("TEARS_OF_GUTHIX")),
        new Destination("Wintertodt Camp", new Coordinate(1624, 3938, 0))
    }),
    NECKLACE_OF_PASSAGE("Necklace of passage", 20, new Destination[] {
        new Destination("Wizards' Tower", new Coordinate(3114, 3179, 0)),
        new Destination("The Outpost", new Coordinate(2430, 3348, 0)),
        new Destination("Eagles' Eyrie", new Coordinate(3405, 3157, 0))
    }),
    SLAYER_RING("Slayer ring", 30, Pattern.compile("(Rub|Teleport)"), new Destination[] {
        new Destination(Pattern.compile("(Teleport|Stronghold Slayer Cave)"), new Coordinate(2432, 3423, 0)),
        new Destination(Pattern.compile("(Teleport|Slayer Tower)"), new Coordinate(3422, 3537, 0)),
        new Destination(Pattern.compile("(Teleport|Fremennik Slayer Dungeon)"), new Coordinate(2802, 10000, 0)),
        new Destination(Pattern.compile("(Teleport|Tarn's Lair)"), new Coordinate(3185, 4601, 0)),
        new Destination(Pattern.compile("(Teleport|Dark Beasts)"), new Coordinate(2028, 4636, 0))
    }),
    DIGSITE_PENDANT("Digsite pendant", 20, new Destination[] {
        new Destination("Digsite", new Coordinate(3341, 3445, 0)),
        new Destination("Fossil Island", new Coordinate(3764, 3869, 1)),
        new Destination("Lithkren", new Coordinate(3549, 10456, 0))
    }),
    COMBAT_BRACELET("Combat bracelet", 30, new Destination[] {
        new Destination("Warriors' Guild", new Coordinate(2882, 3548, 0)),
        new Destination("Champions' Guild", new Coordinate(3191, 3367, 0)),
        new Destination("Monastery", new Coordinate(3052, 3488, 0)),
        new Destination("Ranging Guild", new Coordinate(2655, 3441, 0))
    }),
    SKILLS_NECKLACE("Skills necklace", 30, new Destination[] {
        new Destination("Fishing Guild", new Coordinate(2611, 3390, 0)),
        new Destination("Mining Guild", new Coordinate(3050, 9763, 0)),
        new Destination("Crafting Guild", new Coordinate(2933, 3295, 0)),
        new Destination("Cooking Guild", new Coordinate(3143, 3440, 0)),
        new Destination("Woodcutting Guild", new Coordinate(1662, 3505, 0)),
        new Destination("Farming Guild", new Coordinate(1249, 3718, 0)),
    }),
    RING_OF_WEALTH("Ring of wealth", 30, new Destination[] {
        new Destination("Grand Exchange", new Coordinate(3163, 3478, 0)),
        new Destination(Pattern.compile("Falador(?: Park)?"), new Coordinate(2996, 3375, 0)),
    }),
    AMULET_OF_GLORY("Amulet of glory", 30, new Destination[] {
        new Destination("Edgeville", new Coordinate(3087, 3496, 0)),
        new Destination("Karamja", new Coordinate(2918, 3176, 0)),
        new Destination("Draynor Village", new Coordinate(3105, 3251, 0)),
        new Destination("Al Kharid", new Coordinate(3293, 3163, 0)),
    }),
    RING_OF_SHADOWS(Pattern.compile("^Ring of shadows$"), 20, Pattern.compile("^Teleport$"), new Destination[] {
        new Destination("The Ancient Vault", new Coordinate(3295, 6435, 0)),
        new Destination("The Scar", new Coordinate(2017, 6436, 0), new VarbitRequirement(15141, 1)),
        new Destination("Lassar Undercity", new Coordinate(2588, 6435, 0), new VarbitRequirement(15139, 1)),
//        new Destination("Ghorrock Dungeon", new Coordinate(2588, 6435, 0), new VarbitRequirement(15137, 1)), //TODO
        new Destination("The Stranglewood", new Coordinate(1175, 3423, 0), new VarbitRequirement(15140, 1)), //TODO
    });

    private final Pattern name;
    private final int maxWildyDepth;
    private final Destination[] destinations;
    private final Pattern action;

    ChargedItem(final Pattern name, int maxWildyDepth, final Destination[] destinations) {
        this.name = name;
        this.action = Pattern.compile("Rub");
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
    }

    ChargedItem(final String name, int maxWildyDepth, final Destination[] destinations) {
        this.name = Pattern.compile(name + " ?\\(\\d+\\)");
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
        this.action = Pattern.compile("Rub");
    }

    ChargedItem(final String name, int maxWildyDepth, final Pattern action, final Destination[] destinations) {
        this.name = Pattern.compile(name + " ?\\(\\d+\\)");
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
        this.action = action;
    }

    ChargedItem(final Pattern name, int maxWildyDepth, final Pattern action, final Destination[] destinations) {
        this.name = name;
        this.maxWildyDepth = maxWildyDepth;
        this.destinations = destinations;
        this.action = action;
    }

    private List<BasicItemTransport> asEquippedTransports() {
        final var results = new ArrayList<BasicItemTransport>();
        var requirement = Requirements.and(
            new EquipmentRequirement(getName()),
            new WildernessRequirement(getMaxWildyDepth()),
            new TeleportsEnabled()
        );
        for (var dest : getDestinations()) {
            results.add(
                BasicItemTransport.builder()
                    .itemName(getName())
                    .itemAction(dest.getName())
                    .destination(dest.getPosition())
                    .origin(BasicItemTransport.Origin.EQUIPMENT)
                    .requirement(requirement.and(dest.getRequirement()))
                    .build()
            );
        }
        return results;
    }

    private List<DialogItemTransport> asInventoryTransports() {
        final List<DialogItemTransport> results = new ArrayList<>();
        var requirement = Requirements.and(
            new ItemRequirement(getName(), 1),
            new WildernessRequirement(getMaxWildyDepth()),
            new TeleportsEnabled()
        );
        //When in inventory, "Rub" > select dialog option
        for (final var dest : getDestinations()) {
            var transport = DialogItemTransport.builder()
                .destination(dest.getPosition())
                .itemName(getName())
                .itemAction(getAction())
                .optionPattern(dest.getName())
                .origin(BasicItemTransport.Origin.INVENTORY)
                .requirement(requirement.and(dest.getRequirement()))
                .build();
            results.add(transport);
        }

        return results;
    }

    public static List<BasicItemTransport> getEquippedTransports() {
        return Arrays.stream(values()).flatMap(v -> v.asEquippedTransports().stream()).collect(Collectors.toList());
    }

    public static List<DialogItemTransport> getInventoryTransports() {
        return Arrays.stream(values()).flatMap(v -> v.asInventoryTransports().stream()).collect(Collectors.toList());
    }

    @Getter
    @Value
    @AllArgsConstructor
    public static class Destination {

        @NonNull Pattern name;
        @NonNull Coordinate position;
        @NonNull Requirement requirement;

        public Destination(@NonNull final Pattern pattern, @NonNull final Coordinate position) {
            this(pattern, position, Requirements.none());
        }

        public Destination(@NonNull final String name, @NonNull final Coordinate position) {
            this(name, position, Requirements.none());
        }

        public Destination(@NonNull final String name, @NonNull final Coordinate position, @NonNull final Requirement requirement) {
            this(Pattern.compile(name), position, requirement);
        }
    }
}
